# Jirror - Laravel package to persist Jira data

## Installation
`composer require comdatia/jirror`

The package should add migrations for the new tables, run migrate to create the new database tables.

`php artisan migrate`

