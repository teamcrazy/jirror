<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteImage;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorResolution extends Model
{
    use PersistsRemoteModel, BelongsToWorkspace;

    protected $fillable = [
        'fkid', 'name', 'description', 'jirror_workspace_id',
    ];

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'name' => 'name',
            'description' => 'description',
        ];
    }
}
