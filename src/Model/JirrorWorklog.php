<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorWorklog extends Model
{
    use PersistsRemoteModel,  BelongsToWorkspace;

    protected $fillable = [
        'fkid',
        'jirror_workspace_id',
        'jirror_issue_id',
        'author_id',
        'updater_id',
        'comment',
        'remote_updated_at',
        'started_at',
        'time_spent_seconds',
    ];

    protected function onPersistingRemote($remoteModel, \Comdatia\Jirror\Client $remoteClient, $cascadeChildren = false)
    {
        $this->jirror_workspace_id = JirrorWorkspace::fromUrl($remoteModel->self)->id;
        $issue = JirrorIssue::where('fkid', $remoteModel->issueId)->where('jirror_workspace_id', $this->jirror_workspace_id)->first()->id;
        $this->jirror_issue_id = $issue;

        $this->author_id = JirrorUser::persist($remoteModel->author, $remoteClient)->id;
        $this->updater_id = JirrorUser::persist($remoteModel->updateAuthor, $remoteClient)->id;
        $this->comment = property_exists($remoteModel, 'comment') ? json_encode($remoteModel->comment) : null;
        $this->remote_updated_at = new \Carbon\Carbon($remoteModel->updated);
        $this->started_at = new \Carbon\Carbon($remoteModel->started);
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'time_spent_seconds' => 'timeSpentSeconds',
        ];
    }
}
