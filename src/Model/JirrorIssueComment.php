<?php

namespace Comdatia\Jirror\Model;

use Carbon\Carbon;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorIssueComment extends Model
{
    use PersistsRemoteModel;

    protected $fillable = [
        'fkid', 'jirror_issue_id', 'author_id', 'updater_id', 'remote_created_at', 'remote_updated_at', 'body',
    ];

    public function hydrateFromRemote($remoteModel, $remoteClient)
    {
        $this->fkid = $remoteModel->id;

        preg_match("/\/issue\/([^\/]+)\//", $remoteModel->self, $matches);
        $issueId = $matches[1];
        $this->jirror_issue_id = JirrorIssue::where('fkid', $issueId)->orWhere('key', $issueId)->first()->id;
        $this->author_id = JirrorUser::persist($remoteModel->author, $remoteClient)->id;
        $this->updater_id = JirrorUser::persist($remoteModel->updateAuthor, $remoteClient)->id;
        $this->remote_created_at = new Carbon($remoteModel->created);
        $this->remote_updated_at = new Carbon($remoteModel->updated);
        $this->body = json_encode($remoteModel->body);
    }

    public static function findByRemoteKey($fkid, $remoteModel)
    {
        preg_match("/\/issue\/([^\/]+)\//", $remoteModel->self, $matches);
        $issueId = $matches[1];
        $issueId = JirrorIssue::where('fkid', $issueId)->orWhere('key', $issueId)->first()->id;

        return self::where('fkid', $fkid)->where('jirror_issue_id', $issueId)->first();
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
        ];
    }
}
