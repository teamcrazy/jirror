<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Configuration\ParamConfiguration;
use Illuminate\Database\Eloquent\Model;
use JiraRestApi\User\UserService;

class JirrorWorkspace extends Model
{
    protected $fillable = [
        'name', 'url',
    ];

    public function users()
    {
        return $this->belongsToMany(JirrorUser::class);
    }

    public static function fromUrl($url)
    {
        // Work out which workspace we belong to based on base URL
        $remoteHost = parse_url($url, PHP_URL_HOST);

        return  self::where('url', 'LIKE', "%$remoteHost%")->first();
    }

    public function ensureStructureItems($remoteClient)
    {
        // Link Types
        $types = json_decode($remoteClient->exec('issueLinkType'));
        foreach ($types->issueLinkTypes as $type) {
            JirrorIssueLinkType::persist($type, $remoteClient);
        }

        // Statuses
        $statuses = json_decode($remoteClient->exec('status'));
        foreach ($statuses as $status) {
            JirrorIssueStatus::persist($status, $remoteClient);
        }

        // Issue Types
        $issueTypes = json_decode($remoteClient->exec('issuetype'));
        foreach ($issueTypes as $type) {
            JirrorIssueType::persist($type, $remoteClient);
        }

        // Priorities
        $priorities = json_decode($remoteClient->exec('priority'));
        foreach ($priorities as $priority) {
            JirrorPriority::persist($priority, $remoteClient);
        }

        // Resolutions
        $resolutions = json_decode($remoteClient->exec('resolution'));
        foreach ($resolutions as $resolution) {
            JirrorResolution::persist($resolution, $remoteClient);
        }
    }

    public function getAuthUser()
    {
        foreach ($this->users as $user) {
            $config = new ParamConfiguration($this, $user->username, $user->access_token);
            try {
                $client = new UserService($config);
                $data = $client->getMyself();

                return $user;
            } catch (\Exception $e) {
                // Detatch this user and try the next one
                $this->users()->detach($user->id);
            }
        }
    }
}
