<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteImage;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorPriority extends Model
{
    use PersistsRemoteModel, PersistsRemoteImage, BelongsToWorkspace;

    protected $fillable = [
        'fkid', 'color', 'description', 'icon', 'name', 'jirror_workspace_id',
    ];

    protected $images = ['icon'];

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'color' => 'statusColor',
            'name' => 'name',
            'description' => 'description',
            'icon' => 'iconUrl',
        ];
    }
}
