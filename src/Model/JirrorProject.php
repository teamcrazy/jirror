<?php

namespace Comdatia\Jirror\Model;

use Carbon\Carbon;
use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteImage;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Comdatia\Toggl\Model\Workspace;
use Illuminate\Database\Eloquent\Model;

class JirrorProject extends Model
{
    use PersistsRemoteModel {
        hydrateFromRemote as traitHydrateFromRemote;
    }

    use PersistsRemoteImage, BelongsToWorkspace;

    protected $fillable = [
        'fkid', 'name', 'avatar', 'description', 'lead_id',
    ];

    protected $images = [
        'avatar',
    ];

    public function hydrateFromRemote($remoteModel, $remoteClient)
    {
        $this->traitHydrateFromRemote($remoteModel, $remoteClient);
        if (property_exists($remoteModel, 'lead') && $remoteModel->lead) {
            $leadObj = json_decode(json_encode($remoteModel->lead));
            $remoteUser = JirrorUser::persist($leadObj, $this->remoteClient());
            $this->lead_id = $remoteUser->id;
        }
    }

    protected function onPersistingRemote($remoteModel, \Comdatia\Jirror\Client $remoteClient, $cascadeChildren = false)
    {
        if ($cascadeChildren) {
            // We are a stub from a list, expand all the data
            if (! property_exists($remoteModel, 'issueTypes') || ! is_array($remoteModel->issueTypes)) {
                $remoteModel = $remoteClient->getProject($this->fkid);
                $this->hydrateFromRemote($remoteModel, $remoteClient);
            }
        }
    }

    protected function onPersistedRemote($remoteModel, $remoteClient, $cascadeChildren = false)
    {
        if ($cascadeChildren) {
            // Components
            $components = $remoteClient->getPagedResults('project//'.$this->name.'//component', 'values');

            foreach ($components as $component) {
                JirrorComponent::persist($component, $remoteClient);
            }

            // Issues
            $lastUpdateString = new Carbon($this->last_synced ?? '1990-01-01');
            $lastUpdateString = $lastUpdateString->sub('2 minutes')->format('Y/m/d H:i');
            $this->last_synced = new Carbon();
            $issues = $remoteClient->searchIssues(['project' => $this->name, 'updated' => ['>', '"'.$lastUpdateString.'"']]);
            foreach ($issues as $issue) {
                \Comdatia\Jirror\Model\JirrorIssue::persist($issue, $remoteClient, $cascadeChildren);
            }
            // Don't save the new sync date until finished
            $this->save();
        }
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'name' => 'key',
            'avatar' => ['avatarUrls' => '48x48'],
            'description' => 'name',
        ];
    }
}
