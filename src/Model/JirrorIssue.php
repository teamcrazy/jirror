<?php

namespace Comdatia\Jirror\Model;

use Carbon\Carbon;
use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorIssue extends Model
{
    use PersistsRemoteModel, BelongsToWorkspace;

    protected $fillable = [
        'jirror_issue_type_id',
        'jirror_project_id',
        'jirror_resolution_id',
        'resolution_date',
        'last_viewed',
        'remote_created',
        'remote_updated',
        'jirror_priority_id',
        'labels',
        'estimate',
        'assignee_id',
        'jirror_issue_status_id',
        'comments',
        'description',
        'summary',
        'creator_id',
        'reporter_id',
        'duedate',
    ];

    public static function findByRemoteKey($fkid, $remoteModel)
    {
        $project = JirrorProject::findByRemoteKey($remoteModel->fields->project->id, $remoteModel->fields->project)->first();

        return self::where('fkid', $fkid)->where('jirror_project_id', $project->id)->first();
    }

    protected function onPersistingRemote($remoteModel, \Comdatia\Jirror\Client $remoteClient, $cascadeChildren = false)
    {
        $fields = $remoteModel->fields;

        $dtzFields = ['resolution_date', 'remote_created', 'remote_updated', 'duedate', 'last_viewed'];
        foreach ($dtzFields as $field) {
            if (! is_null($this->$field)) {
                $this->$field = new Carbon($this->$field);
            }
        }

        $this->description = json_encode($this->description);

        $this->jirror_issue_type_id = JirrorIssueType::findByRemoteKey($fields->issuetype->id, $remoteModel->fields->issuetype)->id;
        $this->jirror_project_id = JirrorProject::findByRemoteKey($fields->project->id, $remoteModel->fields->project)->id;
        if (! is_null($fields->resolution)) {
            $this->jirror_resolution_id = JirrorResolution::findByRemoteKey($fields->resolution->id, $fields->resolution)->id;
        } else {
            $this->jirror_resolution_id = null;
        }

        if (! is_null($remoteModel->fields->priority)) {
            $this->jirror_priority_id = JirrorPriority::findByRemoteKey($fields->priority->id, $fields->priority)->id;
        } else {
            $this->jirror_priority_id = null;
        }
        $this->labels = implode(',', $remoteModel->fields->labels);

        if (! is_null($remoteModel->fields->assignee)) {
            $this->assignee_id = JirrorUser::persist($remoteModel->fields->assignee, $remoteClient, false)->id;
        } else {
            $this->assignee_id = null;
        }

        $this->jirror_issue_status_id = JirrorIssueStatus::findByRemoteKey($fields->status->id, $fields->status)->id;
        $this->creator_id = JirrorUser::persist($remoteModel->fields->creator, $remoteClient)->id;
        $this->reporter_id = JirrorUser::persist($remoteModel->fields->reporter, $remoteClient)->id;
    }

    protected function onPersistedRemote($remoteModel, $remoteClient, $cascadeChildren = false)
    {
        $localComponents = [];
        foreach ($remoteModel->fields->components as $component) {
            $localComponents[] = JirrorComponent::persist($component, $remoteClient)->id;
        }
        $this->components()->sync($localComponents);

        if ($cascadeChildren) {
            // Retrieve the entire issue
            $remoteModel = json_decode($remoteClient->exec('issue//'.$this->fkid));

            // Comments first
            foreach ($remoteModel->fields->comment->comments as $comment) {
                JirrorIssueComment::persist($comment, $remoteClient, $cascadeChildren);
            }

            foreach ($remoteModel->fields->attachment as $attachment) {
                JirrorIssueAttachment::persist($attachment, $remoteClient, $cascadeChildren, $this);
            }
        }
    }

    public function issue_type()
    {
        return $this->hasOne(JirrorIssueType::class);
    }

    public function project()
    {
        return $this->hasOne(JirrorProject::class);
    }

    public function resolution()
    {
        return $this->hasOne(JirrorResolution::class);
    }

    public function priority()
    {
        return $this->hasOne(JirrorPriority::class);
    }

    public function assignee()
    {
        return $this->hasOne(JirrorUser::class, 'id', 'assignee_id');
    }

    public function status()
    {
        return $this->hasOne(JirrorIssueStatus::class);
    }

    public function creator()
    {
        return $this->hasOne(JirrorUser::class, 'id', 'creator_id');
    }

    public function reporter()
    {
        return $this->hasOne(JirrorUser::class, 'id', 'reporter_id');
    }

    public function components()
    {
        return $this->belongsToMany(JirrorComponent::class);
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'key' => 'key',
            'resolution_date' => ['fields' => 'resolutiondate'],
            'last_viewed' => ['fields' => 'lastViewed'],
            'remote_created' => ['fields' => 'created'],
            'remote_updated' => ['fields' => 'updated'],
            'estimate' => ['fields' => 'timeestimate'],
            'description' => ['fields' => 'description'],
            'summary' => ['fields' => 'summary'],
            'duedate' => ['fields' => 'duedate'],
        ];
    }
}
