<?php

namespace Comdatia\Jirror\Model;

use Carbon\Carbon;
use Comdatia\Jirror\Traits\PersistsRemoteImage;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorIssueAttachment extends Model
{
    use PersistsRemoteModel, PersistsRemoteImage;

    protected $fillable = [
        'fkid',
        'filename',
        'author_id',
        'remote_created',
        'mime_type',
        'size',
        'filepath',
        'jirror_issue_id',
    ];

    protected $images = ['filepath'];

    public function hydrateFromRemote($remoteModel, $remoteClient)
    {
        $this->fkid = $remoteModel->id;
        $this->filename = $remoteModel->filename;
        $this->author_id = JirrorUser::persist($remoteModel->author, $remoteClient)->id;
        $this->remote_created_at = new Carbon($remoteModel->created);
        $this->mime_type = $remoteModel->mimeType;
        $this->size = $remoteModel->size;
        $this->filepath = $remoteModel->content;
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
        ];
    }

    public static function persist($remoteModel, $remoteClient, $cascadeChildren = false, $issue = null)
    {
        static::$remoteClient = $remoteClient;
        $remoteId = static::remoteId($remoteModel);
        $model = static::findByRemoteKey($remoteId, $remoteModel);
        if (! $model) {
            $model = new static();
        }
        $fkField = static::fkField();
        $model->$fkField = $remoteId;
        $model->hydrateFromRemote($remoteModel, $remoteClient);
        $model->jirror_issue_id = $issue->id;
        $model->fireModelEvent('persisting_remote');
        $model->onPersistingRemote($remoteModel, $remoteClient, $cascadeChildren);
        $model->save();
        $model->fireModelEvent('persisted_remote');
        $model->onPersistedRemote($remoteModel, $remoteClient, $cascadeChildren);

        return $model;
    }
}
