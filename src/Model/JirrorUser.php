<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\PersistsRemoteImage;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorUser extends Model
{
    use PersistsRemoteModel, PersistsRemoteImage;

    protected $fillable = [
        'fkid', 'username', 'avatar', 'email', 'display_name', 'time_zone', 'access_token', 'user_id',
    ];

    protected $images = [
        'avatar',
    ];

    public function workspaces()
    {
        return $this->belongsToMany(JirrorWorkspace::class);
    }

    protected function onPersistingRemote($remoteModel, \Comdatia\Jirror\Client $remoteClient, $cascadeChildren = false)
    {
        // Prevent linkages wiping out data
        if ($this->username === null || $this->username == '') {
            $this->username = $this->getOriginal('username');
            $this->email = $this->getOriginal('email');
            $this->time_zone = $this->getOriginal('time_zone');

            if ($this->username == null) {
                $this->username = $remoteModel->accountId;
            }
        }
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'accountId',
            'username' => 'name',
            'avatar' => ['avatarUrls' => '48x48'],
            'email' => ['emailAddress'],
            'display_name' => 'displayName',
            'time_zone' => 'timeZone',
        ];
    }
}
