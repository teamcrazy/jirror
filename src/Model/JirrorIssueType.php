<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteImage;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorIssueType extends Model
{
    use PersistsRemoteModel {
        hydrateFromRemote as traitHydrateFromRemote;
    }

    use PersistsRemoteImage, BelongsToWorkspace;

    protected $fillable = [
        'fkid', 'name', 'description', 'icon', 'is_subtask',
    ];

    protected $images = [
        'icon',
    ];

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'name' => 'name',
            'description' => 'description',
            'icon' => 'iconUrl',
            'is_subtask' => 'subtask',
        ];
    }
}
