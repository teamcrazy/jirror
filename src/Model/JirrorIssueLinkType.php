<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorIssueLinkType extends Model
{
    use PersistsRemoteModel, BelongsToWorkspace;

    protected $fillable = [
        'fkid', 'name', 'inward', 'outward', 'jirror_workspace_id',
    ];

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'name' => 'name',
            'inward' => 'inward',
            'outward' => 'outward',
        ];
    }
}
