<?php

namespace Comdatia\Jirror\Model;

use Comdatia\Jirror\Traits\BelongsToWorkspace;
use Comdatia\Jirror\Traits\PersistsRemoteModel;
use Illuminate\Database\Eloquent\Model;

class JirrorComponent extends Model
{
    use PersistsRemoteModel,  BelongsToWorkspace;

    protected $fillable = [
        'fkid', 'name', 'jirror_workspace_id',
    ];

    protected $images = ['icon'];

    protected function onPersistingRemote($remoteModel, \Comdatia\Jirror\Client $remoteClient, $cascadeChildren = false)
    {
        if (property_exists($remoteModel, 'project')) {
            $this->jirror_project_id = JirrorProject::where('name', $remoteModel->project)->first()->id;
        }
    }

    public static function remoteMappings()
    {
        return [
            'fkid' => 'id',
            'name' => 'name',
        ];
    }
}
