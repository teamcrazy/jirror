<?php

namespace Comdatia\Jirror\Traits;

use Comdatia\Jirror\Client;
use JsonSerializable;
use PDO;

trait PersistsRemoteModel
{
    protected static $remoteClient;
    protected $remoteModel;

    public static function remoteClient()
    {
        return static::$remoteClient;
    }

    public function initializePersistsRemoteModel()
    {
        $this->addObservableEvents(['persisting_remote', 'persisted_remote', 'hydrated_remote']);
    }

    public static function persistingRemote($callback)
    {
        static::registerModelEvent('persisting_remote', $callback);
    }

    public static function persistedRemote($callback)
    {
        static::registerModelEvent('persisted_remote', $callback);
    }

    public static function hydratedRemote($callback)
    {
        static::registerModelEvent('hydrated_remote', $callback);
    }

    public function hydrateFromRemote($remoteModel, $remoteClient)
    {
        $mappings = static::remoteMappings();
        foreach ($mappings as $localField => $remoteField) {
            $value = static::retrieveProperty($remoteModel, $remoteField);
            $this->$localField = $value;
        }
        $this->remoteModel = $remoteModel;

        // Call any trait hydrations
        $this->callTraitMethod('hydrateFromRemote', [$remoteModel, $remoteClient]);
        $this->fireModelEvent('hydrated_remote');
    }

    public static function findByRemoteKey(&$fkid, &$remoteModel)
    {
        $query = static::where(static::fkField(), $fkid);
        $params = [$fkid, $remoteModel, $query];
        $query = static::callTraitStatic('findByRemoteKey', [$query, $fkid, $remoteModel]);

        return $query->first();
    }

    public static function fkField()
    {
        return 'fkid';
    }

    public static function remoteId($fromModel)
    {
        $fkid = static::fkField();
        $mappings = static::remoteMappings();

        return static::retrieveProperty($fromModel, $mappings[$fkid]);
    }

    /**
     * In case the implementing model wants to do anything immediately after persisting
     * This is done outside of Laravel's event model so we can pass additional parameters.
     * @param JsonSerializable|StdClass $remoteModel
     * @param Client $remoteClient
     * @param bool $cascadeChildren
     * @return void
     */
    protected static function onPersistingRemote($remoteModel, Client $remoteClient, $cascadeChildren = false)
    {
    }

    /**
     * @param $remoteModel
     * @param Client $remoteClient
     * @param bool $cascadeChildren
     * @return void
     */
    protected static function onPersistedRemote($remoteModel, $remoteClient, $cascadeChildren = false)
    {
        // In case the implementing model wants to do anything immediately after persisting
        // This is done outside of Laravel's event model so we can pass additional parameters
    }

    public static function persist($remoteModel, $remoteClient, $cascadeChildren = false)
    {
        static::$remoteClient = $remoteClient;
        $remoteId = static::remoteId($remoteModel);
        $model = static::findByRemoteKey($remoteId, $remoteModel);
        if (! $model) {
            $model = new static();
        }
        $fkField = static::fkField();
        $model->$fkField = $remoteId;
        $model->hydrateFromRemote($remoteModel, $remoteClient);
        $model->fireModelEvent('persisting_remote');
        $model->onPersistingRemote($remoteModel, $remoteClient, $cascadeChildren);
        $model->save();
        $model->fireModelEvent('persisted_remote');
        $model->onPersistedRemote($remoteModel, $remoteClient, $cascadeChildren);

        return $model;
    }

    abstract public static function remoteMappings();

    protected static function retrieveProperty($fromModel, $fieldName)
    {
        if (is_array($fromModel)) {
            $fromModel = json_decode(json_encode($fromModel));
        }

        if (! is_object($fromModel)) {
            echo 'Bad';
            var_dump(($fromModel));
        }
        if (is_array($fieldName)) {
            $prop = array_key_first($fieldName);
            $subProp = $fieldName[$prop];

            return property_exists($fromModel, $prop) ? static::retrieveProperty($fromModel->$prop, $subProp) : null;
        } else {
            return property_exists($fromModel, $fieldName) ? $fromModel->$fieldName : null;
        }
    }

    protected function callTraitMethod($methodName, $args)
    {
        $traits = get_declared_traits();
        foreach ($traits as $trait) {
            $trait = class_basename($trait);
            $traitMethodName = 'trait'.$trait.ucfirst($methodName);
            if (method_exists($this, $traitMethodName)) {
                call_user_func([$this, $traitMethodName], ...$args);
            }
        }
    }

    protected static function callTraitStatic($methodName, $args)
    {
        $traits = get_declared_traits();
        foreach ($traits as $trait) {
            $trait = class_basename($trait);
            $traitMethodName = 'trait'.$trait.ucfirst($methodName);
            if (method_exists(static::class, $traitMethodName)) {
                $result = forward_static_call([static::class, $traitMethodName], ...$args);
                if ($result) {
                    $args[0] = $result;
                }
            }
        }

        return $args[0];
    }
}
