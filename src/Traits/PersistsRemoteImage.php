<?php

namespace Comdatia\Jirror\Traits;

use Illuminate\Support\Facades\Storage;

trait PersistsRemoteImage
{
    public static function bootPersistsRemoteImage()
    {
        static::persistedRemote(function ($model) {
            $model->saveRemoteImages();
        });
    }

    public function saveRemoteImages()
    {
        if (property_exists($this, 'images') && is_array($this->images)) {
            foreach ($this->images as $imgField) {
                $value = $this->$imgField;
                if (strpos($value, 'http') === 0) {
                    $this->downloadImageField($imgField, $value);
                }
            }
            $this->save();
        }
    }

    protected function downloadImageField($imgField, $uri)
    {
        $disk = Storage::disk('local');

        $baseFolder = strtolower(class_basename(get_class($this))).'/'.$this->id;
        if (! $disk->exists($baseFolder)) {
            $disk->makeDirectory($baseFolder);
        }

        $fullPath = $disk->getDriver()->getAdapter()->getPathPrefix()."/$baseFolder";
        $client = static::remoteClient();

        $response = $client->download($uri, $fullPath, $imgField);
        $this->$imgField = $baseFolder."/$imgField";
    }
}
