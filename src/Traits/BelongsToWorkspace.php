<?php

namespace Comdatia\Jirror\Traits;

use Comdatia\Jirror\Model\JirrorWorkspace;

trait BelongsToWorkspace
{
    public function workspace()
    {
        return $this->belongsTo(JirrorWorkspace::class);
    }

    public function traitBelongsToWorkspaceHydrateFromRemote($remoteModel, $remoteClient)
    {
        $this->jirror_workspace_id = JirrorWorkspace::fromUrl($remoteModel->self)->id;
    }

    public static function traitBelongsToWorkspaceFindByRemoteKey($query, $fkId, $remoteModel)
    {
        $workspace = JirrorWorkspace::fromUrl($remoteModel->self);
        $query->where('jirror_workspace_id', $workspace->id);

        return $query;
    }
}
