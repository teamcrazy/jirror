<?php

namespace Comdatia\Jirror\Configuration;

use Comdatia\Jirror\Model\JirrorWorkspace;
use JiraRestApi\Configuration\DotEnvConfiguration;

class ParamConfiguration extends DotEnvConfiguration
{
    public function __construct(
        JirrorWorkspace $jirrorWorkspace,
        $username,
        $accessToken
    ) {
        $this->jiraHost = $jirrorWorkspace->url;
        $this->jiraUser = $username;
        $this->jiraPassword = $accessToken;
        $this->jiraLogEnabled = env('JIRA_LOG_ENABLED', true);
        $this->jiraLogFile = env('JIRA_LOG_FILE', 'jira-rest-client.log');
        $this->jiraLogLevel = env('JIRA_LOG_LEVEL', 'WARNING');
        $this->curlOptSslVerifyHost = env('CURLOPT_SSL_VERIFYHOST', false);
        $this->curlOptSslVerifyPeer = env('CURLOPT_SSL_VERIFYPEER', false);
        $this->curlOptUserAgent = env('CURLOPT_USERAGENT', $this->getDefaultUserAgentString());
        $this->curlOptVerbose = env('CURLOPT_VERBOSE', false);
        $this->proxyServer = env('PROXY_SERVER');
        $this->proxyPort = env('PROXY_PORT');
        $this->proxyUser = env('PROXY_USER');
        $this->proxyPassword = env('PROXY_PASSWORD');

        $this->useV3RestApi = env('JIRA_REST_API_V3', true);
    }
}
