<?php

namespace Comdatia\Jirror\Providers;

use Comdatia\Jirror\Console\Commands\AddJirrorUser;
use Comdatia\Jirror\Console\Commands\AddJirrorWorkspace;
use Comdatia\Jirror\Console\Commands\SyncData;
use Illuminate\Support\ServiceProvider;

class JirrorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        if ($this->app->runningInConsole()) {
            $this->commands([
                AddJirrorWorkspace::class,
                AddJirrorUser::class,
                SyncData::class,
            ]);
        }
    }
}
