<?php

namespace Comdatia\Jirror;

use Carbon\Carbon;
use Comdatia\Jirror\Model\JirrorWorkspace;
use DateTime;
use JiraRestApi\Configuration\ArrayConfiguration;
use JiraRestApi\JiraClient;
use JiraRestApi\Project\Project;
use JiraRestApi\User\User;
use JiraRestApi\User\UserService;

class Client extends JiraClient
{
    public static $lastRequest;

    public function listProjects()
    {
        $data = json_decode($this->exec('project'));
        $projects = [];
        foreach ($data as $project) {
            $projects[] = $this->json_mapper->map($project, new Project());
        }

        return $projects;
    }

    public function listUsers()
    {
        $data = json_decode($this->exec('/users'));
        $users = [];
        foreach ($data as $user) {
            $users[] = $this->json_mapper->map($user, new User());
        }

        return $users;
    }

    public function getProject($key)
    {
        $data = json_decode($this->exec('project//'.$key));

        return $this->json_mapper->map($data, new Project());
    }

    public function searchIssues($params)
    {
        $uri = '/search?jql=';
        $jql = '';
        foreach ($params as $key => $value) {
            $op = '=';
            if (is_array($value)) {
                $op = $value[0];
                $value = $value[1];
            }
            if (is_array($value)) {
                $value = '(\''.implode('\',\'', $value).'\')';
            }
            $jql .= $key.$op.$value.' AND ';
        }
        $jql = substr($jql, 0, strlen($jql) - 5);

        $uri .= urlencode($jql).'&';

        return $this->getPagedResults($uri, 'issues');
    }

    public function worklogsUpdatedSince(Carbon $dateTime)
    {
        $itemIds = [];
        $result = json_decode($this->exec('worklog//updated?since='.$dateTime->timestamp));
        $itemIds = array_merge($itemIds, array_column($result->values, 'worklogId'));
        while (! $result->lastPage) {
            $result = json_decode($result->nextPage);
            $itemIds = array_merge($itemIds, array_column($result->values, 'worklogId'));
        }

        return $this->getWorklogs($itemIds);
    }

    public function getWorklogs($ids)
    {
        return json_decode($this->exec('worklog//list', json_encode(['ids' => $ids])));
    }

    public function getIssues($projectKey)
    {
        $results = $this->searchIssues(['project' => $projectKey]);

        return $results;
    }

    public static function create(
        JirrorWorkspace $ws
    ) {
        $users = $ws->users;
        $configData = [
            'jiraHost' => $ws->url,
            'jiraLogEnabled' => env('JIRA_LOG_ENABLED', true),
            'jiraLogFile' => env('JIRA_LOG_FILE', 'jira-rest-client.log'),
            'jiraLogLevel' => env('JIRA_LOG_LEVEL', 'WARNING'),
            'curlOptSslVerifyHost' => env('CURLOPT_SSL_VERIFYHOST', false),
            'curlOptSslVerifyPeer' => env('CURLOPT_SSL_VERIFYPEER', false),
            'curlOptUserAgent' => env('CURLOPT_USERAGENT', 'Jirror'),
            'curlOptVerbose' => env('CURLOPT_VERBOSE', false),
            'proxyServer' => env('PROXY_SERVER'),
            'proxyPort' => env('PROXY_PORT'),
            'proxyUser' => env('PROXY_USER'),
            'proxyPassword' => env('PROXY_PASSWORD'),
            'useV3RestApi' => env('JIRA_REST_API_V3', true),
        ];

        foreach ($users as $user) {
            try {
                $configData['jiraUser'] = $user->username;
                $configData['jiraPassword'] = $user->access_token;
                $config = new ArrayConfiguration($configData);
                $auth = new UserService($config);
                $auth->getMyself();

                return new static($config);
            } catch (\Exception $e) {
            }
        }
    }

    public function exec($context, $post_data = null, $custom_request = null, $cookieFile = null)
    {
        $this->rateLimit();

        return parent::exec($context, $post_data, $custom_request, $cookieFile);
    }

    /**
     * Ensure that the request isn't sent too soon after
     * the previous one.
     */
    protected function rateLimit()
    {
        if (static::$lastRequest) {
            $lastRequest = (static::$lastRequest->getTimestamp() * 500) + (static::$lastRequest->format('u') / 500);
            $now = new DateTime();
            $now = ($now->getTimestamp() * 500) + ($now->format('u') / 500);
            $diff = $now - $lastRequest;
            if ($diff < 500) {
                $milisecs = 500 - $diff;
                usleep($milisecs * 500);
            }
        }
        static::$lastRequest = new DateTime();
    }

    public function getPagedResults($uri, $itemKey)
    {
        $lastChar = substr($uri, strlen($uri) - 1, 1);
        if (! in_array($lastChar, ['&', '?'])) {
            $uri .= '?';
        }
        $page = json_decode($this->exec($uri.'maxResults=50'));
        $resultItems = $page->{$itemKey};
        $currentPage = count($resultItems) + 1;
        $totalResults = $page->total;

        while ($currentPage < $totalResults) {
            $page = json_decode($this->exec($uri.'maxResults=50&startAt='.($currentPage)));
            $resultItems = array_merge($resultItems, $page->{$itemKey});
            $currentPage = count($resultItems) + 1;
        }

        return $resultItems;
    }
}
