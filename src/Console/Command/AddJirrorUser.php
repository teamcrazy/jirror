<?php

namespace Comdatia\Jirror\Console\Commands;

use Comdatia\Jirror\Configuration\ParamConfiguration;
use Comdatia\Jirror\Model\JirrorUser;
use Comdatia\Jirror\Model\JirrorWorkspace;
use Exception;
use Illuminate\Console\Command;
use JiraRestApi\Auth\AuthService;
use JiraRestApi\User\UserService;

class AddJirrorUser extends Command
{
    /**
     * @var string
     */
    protected $signature = 'jirror:user:create {workspaceId} {username} {accessToken}';

    /**
     * @var string
     */
    protected $description = 'Register a new Jira user into the application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $workspaceId = $this->argument('workspaceId');
        $workspace = JirrorWorkspace::find($workspaceId);
        if (! $workspace) {
            throw new Exception('Invalid workspace ID');
        }
        $username = $this->argument('username');
        $token = $this->argument('accessToken');

        $config = new ParamConfiguration($workspace, $username, $token);
        $client = new UserService($config);

        $user = $client->getMyself();

        $model = new JirrorUser([
            'fkid' => $user->accountId,
            'username' => $user->emailAddress,
            'email' => $user->emailAddress,
            'access_token' => $token,
        ]);
        $model->save();
        $model->workspaces()->attach($workspace);

        $this->getOutput()->writeln('User created and associated with workspace '.$workspace->name);
    }
}
