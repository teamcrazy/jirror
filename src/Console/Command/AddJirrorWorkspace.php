<?php

namespace Comdatia\Jirror\Console\Commands;

use Comdatia\Jirror\Model\JirrorWorkspace;
use Illuminate\Console\Command;

class AddJirrorWorkspace extends Command
{
    /**
     * @var string
     */
    protected $signature = 'jirror:workspace:create {name} {url}';

    /**
     * @var string
     */
    protected $description = 'Register a new Jira workspace into the application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $url = $this->argument('url');

        $model = new JirrorWorkspace(['name' =>$name, 'url'=>$url]);
        $model->save();
        $this->getOutput()->writeln('Created new workspace with id '.$model->id);
    }
}
