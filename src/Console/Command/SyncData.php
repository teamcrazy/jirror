<?php

namespace Comdatia\Jirror\Console\Commands;

use Carbon\Carbon;
use Comdatia\Jirror\Client;
use Comdatia\Jirror\Configuration\ParamConfiguration;
use Comdatia\Jirror\Model\JirrorProject;
use Comdatia\Jirror\Model\JirrorUser;
use Comdatia\Jirror\Model\JirrorWorklog;
use Comdatia\Jirror\Model\JirrorWorkspace;
use Exception;
use Illuminate\Console\Command;

class SyncData extends Command
{
    /**
     * @var string
     */
    protected $signature = 'jirror:data:sync';

    /**
     * @var string
     */
    protected $description = 'Sync new data to the application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (JirrorWorkspace::all() as $ws) {
            $user = $ws->getAuthUser();
            $client = new Client(new ParamConfiguration($ws, $user->username, $user->access_token));
            $projects = $client->listProjects();
            foreach ($projects as $idx => $project) {
                $this->output->writeln('Syncing '.$project->key.' ('.($idx + 1).' of '.count($projects).')');
                JirrorProject::persist($project, $client, true);
            }

            // Worklogs
            $lastSync = new Carbon($ws->last_synced ?? '1990-01-01');
            $ws->last_synced = new Carbon();
            $worklogs = $client->worklogsUpdatedSince($lastSync);
            foreach ($worklogs as $worklog) {
                JirrorWorklog::persist($worklog, $client);
            }
            $ws->save();
        }
    }
}
