<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJirrorTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('jirror_workspaces', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('url');
            $table->timestamps();
            $table->timestamp('last_synced')->nullable();

            $table->index('url');
        });

        Schema::create('jirror_users', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('username');
            $table->string('display_name')->nullable();
            $table->string('avatar')->nullable();
            $table->string('email')->nullable();
            $table->string('access_token')->nullable();
            $table->string('time_zone')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->timestamps();

            $table->index('fkid');
            $table->foreign('user_id')->on('users')->references('id')->cascadeOnDelete();
        });

        Schema::create('jirror_issue_types', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('name');
            $table->string('description');
            $table->string('icon');
            $table->boolean('is_subtask')->default(false);
            $table->foreignId('jirror_workspace_id');
            $table->timestamps();
            $table->index('fkid');
            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces')->cascadeOnDelete();
        });

        Schema::create('jirror_resolutions', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('name');
            $table->string('description');
            $table->foreignId('jirror_workspace_id');
            $table->timestamps();
            $table->index('fkid');
            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces')->cascadeOnDelete();
        });

        Schema::create('jirror_priorities', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('color');
            $table->string('description');
            $table->string('icon');
            $table->string('name');
            $table->foreignId('jirror_workspace_id');
            $table->timestamps();
            $table->index('fkid');
            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces')->cascadeOnDelete();
        });

        Schema::create('jirror_issue_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->foreignId('jirror_workspace_id');
            $table->string('name');
            $table->string('description');
            $table->string('icon');
            $table->timestamps();

            $table->index('fkid');
            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces');
        });

        Schema::create('jirror_projects', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('name');
            $table->string('avatar');
            $table->text('description');
            $table->foreignId('lead_id')->nullable();
            $table->foreignId('jirror_workspace_id');
            $table->timestamps();
            $table->timestamp('last_synced')->nullable();
            $table->index('fkid');

            $table->foreign('lead_id', 'FK_projects_lead_id')->references('id')->on('jirror_users')->onDelete('set null');
            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces')->cascadeOnDelete();
        });

        Schema::create('jirror_issue_link_types', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->foreignId('jirror_workspace_id');
            $table->string('name');
            $table->string('inward');
            $table->string('outward');
            $table->timestamps();

            $table->index('fkid');
            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces')->cascadeOnDelete();
        });

        Schema::create('jirror_components', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('name');
            $table->foreignId('jirror_workspace_id');
            $table->foreignId('jirror_project_id');
            $table->timestamps();

            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces')->cascadeOnDelete();
            $table->foreign('jirror_project_id')->references('id')->on('jirror_projects')->cascadeOnDelete();
        });

        Schema::create('jirror_issues', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('key');
            $table->foreignId('jirror_issue_type_id'); // issuetype
            $table->foreignId('jirror_project_id'); // project
            $table->foreignId('jirror_resolution_id')->nullable();
            $table->dateTimeTz('resolution_date')->nullable(); // resolutiondate
            $table->dateTimeTz('last_viewed')->nullable(); // lastViewed
            $table->dateTimeTz('remote_created')->nullable(); // created
            $table->dateTimeTz('remote_updated')->nullable(); // updated
            $table->foreignId('jirror_priority_id')->nullable(); // priority
            $table->json('labels'); // labels
            $table->bigInteger('estimate')->nullable(); // timeestimate
            $table->foreignId('assignee_id')->nullable();
            $table->foreignId('jirror_issue_status_id');
            $table->foreignId('jirror_workspace_id');
            $table->text('description')->nullable();
            $table->string('summary');
            $table->foreignId('creator_id');
            $table->foreignId('reporter_id');
            $table->dateTimeTz('duedate')->nullable();
            $table->timestamps();

            $table->index('fkid');
            $table->index('key');
            $table->foreign('jirror_issue_type_id')->references('id')->on('jirror_issue_types');
            $table->foreign('jirror_project_id')->references('id')->on('jirror_projects')->cascadeOnDelete();
            $table->foreign('jirror_resolution_id')->references('id')->on('jirror_resolutions');
            $table->foreign('jirror_priority_id')->references('id')->on('jirror_priorities');
            $table->foreign('assignee_id')->references('id')->on('jirror_users');
            $table->foreign('jirror_issue_status_id')->references('id')->on('jirror_issue_statuses');
        });

        Schema::create('jirror_issue_comments', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->foreignId('jirror_issue_id');
            $table->foreignId('author_id');
            $table->foreignId('updater_id');
            $table->dateTimeTz('remote_created_at');
            $table->dateTimeTz('remote_updated_at');
            $table->text('body');
            $table->timestamps();

            $table->index('fkid');
            $table->foreign('jirror_issue_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
            $table->foreign('author_id')->references('id')->on('jirror_users');
            $table->foreign('updater_id')->references('id')->on('jirror_users');
        });

        Schema::create('jirror_issue_attachments', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->string('filename');
            $table->foreignId('author_id');
            $table->dateTimeTz('remote_created_at');
            $table->string('mime_type');
            $table->integer('size');
            $table->text('filepath');
            $table->foreignId('jirror_issue_id');
            $table->timestamps();

            $table->index('fkid');
            $table->foreign('author_id')->references('id')->on('jirror_users');
            $table->foreign('jirror_issue_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
        });

        Schema::create('jirror_worklogs', function (Blueprint $table) {
            $table->id();
            $table->string('fkid');
            $table->foreignId('jirror_workspace_id');
            $table->foreignId('jirror_issue_id');
            $table->foreignId('author_id');
            $table->foreignId('updater_id');
            $table->text('comment')->nullable();
            $table->dateTimeTz('remote_updated_at');
            $table->dateTimeTz('started_at');
            $table->bigInteger('time_spent_seconds');
            $table->timestamps();

            $table->foreign('jirror_workspace_id')->references('id')->on('jirror_workspaces');
            $table->foreign('jirror_issue_id')->references('id')->on('jirror_issues');
            $table->index('fkid');
        });

        Schema::create('jirror_issues_subtasks', function (Blueprint $table) {
            $table->foreignId('issue_parent_id');
            $table->foreignId('issue_child_id');
            $table->unique(['issue_parent_id', 'issue_child_id']);
            $table->foreign('issue_parent_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
            $table->foreign('issue_child_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
        });

        Schema::create('jirror_issues_links', function (Blueprint $table) {
            $table->foreignId('jirror_issue_link_type_id');
            $table->foreignId('jirror_issue_inward_id');
            $table->foreignId('jirror_issue_outward_id');
            $table->timestamps();

            $table->foreign('jirror_issue_link_type_id')->references('id')->on('jirror_issue_link_types')->cascadeOnDelete();
            $table->foreign('jirror_issue_inward_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
            $table->foreign('jirror_issue_outward_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
        });

        Schema::create('jirror_component_jirror_issue', function (Blueprint $table) {
            $table->foreignId('jirror_component_id');
            $table->foreignId('jirror_issue_id');
            $table->foreign('jirror_component_id')->references('id')->on('jirror_components')->cascadeOnDelete();
            $table->foreign('jirror_issue_id')->references('id')->on('jirror_issues')->cascadeOnDelete();
        });

        Schema::create('jirror_user_jirror_workspace', function (Blueprint $table) {
            $table->foreignId('jirror_user_id');
            $table->foreignId('jirror_workspace_id');

            $table->foreign('jirror_user_id')->on('jirror_users')->references('id')->cascadeOnDelete();
            $table->foreign('jirror_workspace_id')->on('users')->references('id')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jirror_issues_subtasks');
        Schema::dropIfExists('jirror_issues_links');
        Schema::dropIfExists('jirror_component_jirror_issue');
        Schema::dropIfExists('jirror_components');
        Schema::dropIfExists('jirror_issue_comments');
        Schema::dropIfExists('jirror_issue_attachments');
        Schema::dropIfExists('jirror_worklogs');
        Schema::dropIfExists('jirror_issues');
        Schema::dropIfExists('jirror_issue_statuses');
        Schema::dropIfExists('jirror_issue_types');
        Schema::dropIfExists('jirror_resolutions');
        Schema::dropIfExists('jirror_priorities');
        Schema::dropIfExists('jirror_issue_link_types');
        Schema::dropIfExists('jirror_user_jirror_workspace');
        Schema::dropIfExists('jirror_projects');
        Schema::dropIfExists('jirror_workspaces');
        Schema::dropIfExists('jirror_users');
    }
}
